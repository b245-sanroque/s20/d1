console.log("Konnichiwa!!");

/*	LOOPS
	- are ONE of the "MOST IMPORTANT" feature that a programming language must have.
	- lets us execute code repeatedly in a pre-set number of time or maybe forever.

	WHILE LOOP
	- takes a single condition. If the condition evaluates to true, the code inside the block will run.
		e.g.
		let count = 5;

		while (count !== 0){
			console.log("While "+count);
			count--;
		}

	DO-WHILE LOOP
	- works alot like while loop but unlike while loops, do-while loops GUARANTEE the code will be EXECUTED AT LEAST ONCE.
		e.g.
		let number = Number(prompt("Give me a number"));

		do{
			console.log("Do While "+number);
			number += 1;
		}
		while (number < 10)


	FOR LOOP
	- MORE FLEXIBLE than while and do-while loops
		e.g.
			for (let count = 0; count <= 20; count++){
				console.log(count);
			}

		THREE PARTS
			1. Initial Value - will track the progression of the loop.
			2. Condition - will evaluate/determine whether the loop will run one more time.
			3. Iteration - indicates how to advance the loop.		
*/

// [SECTION] While Loop
	console.log("[SECTION] While Loop");
/*
	While Loop 
		- takes in an expression/condition.
	
	*Expression/s 
		- is/are any unit of code that can be evaluated to a value.
		- if the condition evaluates to be TRUE, the statements inside the code block will be executed.
		- A loop will iterate a certain number of times until an expression/condition evaluates as false.

	*Iteration
		- is the term given to the repettion of statements.

	SYNTAX:
	while(expression/condition){
		statements;
		increment/decrement;
	}
*/
	let count = 5;

		while(count !== 0){
			console.log("The current value of count is "+count);
			// if we use increment, it will give us an infinite loop.
			// since it will never reach 0.
			// count++;

			count--;
		/*	- decreases the value of count by 1 after every iteration to stop the loop when it reaches 0.
			
			- loops occupy a significant amount of memory space in our devices.
			
			REMINDER: Make sure that expressions/conditions in loops have their corresponding increment/decrement operators to stop the loop.

			* Forgetting to include this is loops will make our applications run a infinite loop

			* After running the script, if a slow response from the browser UIs experienced or an infinite loop is sent in the console, "QUICKLY" CLOSE the application/tab/browser to avoid this.*/
		}

// [SECTION] DO WHILE LOOP
		console.log("[SECTION] DO WHILE LOOP");
/*	
	Do While Loop
	- a do-while loop works like a while loop but unlike while loops, do-while loops guarantee that the code will be executed at least once.

		SYNTAX:
			do{
				statements;
				increment/decerement;
			}while(expression/conditions)
*/
	// Number() - converts input of the user from string to number.

	// let number = Number(prompt("Give me a number:"));

	// do{
	// 	console.log("Do While: "+number);
	// 	number++;
	// 	// number--; //and (-) value results to infinite loop
	// }while (number <10)

// [SECTION] FOR LOOP
		console.log("[SECTION] FOR LOOP");
/*
	For Loop
	- is more flexible than while and do-while loops
	- consists of 3 Parts:
		1. Initialization - value that will track the progression of the loop.
		2. Expression/Condition - that will be evaluated which will determine whether the loop will run one more time.
		3. finalExpression - indicates how the loop will advance.

		SYNTAX:
			for(initialization; expression/condition; finalExpression){
				statement/s;
			}
*/

/*	BUSINESS LOGIC
	1. We will create a loop that will start from zero and end at 20
	2. Every iteration of the loop, the value of count will be checked if it is equal or less than 20.
	3. If the value of count is less than of equal to 20, the statement inside the loop will run.
	4. The value of count will be incremented by one for each iteration.
*/


	for(let count = 0; // initialization
	count <= 20; // condition
	count++){ // increment(finalExpression)
		console.log("The current value of count is :"+count);
	}

	let myString = "Christopher";
console.log("myString");	
console.log(myString);	
		// characters in strings may be counted using the .length propert
		// Strings are special compared to other data types that it has access to functions and other pieces of information.

	console.log(myString.length); // 11, displays how many characters(alphabet) there are in myString

	// Accesing the characters if a string you can also use the index of the letter/character
	console.log(myString[8]); // h ,9th letter
	console.log(myString[myString.length -1]); // r ,last letter


	let myName = "Aira";
console.log("myName");	
console.log(myName);	
	/*
		Create a loop tha will print out the letters of our name individually.

		Console number 3 instead when the letter to be printed out is vowel.
	*/

	for(let i = 0; i < myName.length; i++){
		console.log(myName[i]);
	}

	for(let i = 0; i < myName.length; i++){
		myName = myName.toLowerCase();
		if(myName[i] === "a" || 
			myName[i] === "e" || 
			myName[i] === "i" || 
			myName[i] === "o" || 
			myName[i] === "u"){
			console.log(3);
		}else{
			console.log(myName[i]);
		}
	}

// [SECTION] Continue and Break Statements
	console.log("[SECTION] Continue and Break Statements");
/*
	Continue Statement
	- allows the code to go to the next iteration of the loop without finishing the executuin of all statements in a code block.

	Break Statement
	- is used to terminate the current loop once a match has been found.
*/

	for(let count = 0; count<=20; count++){
		// if we divide count to 2 and if the remainder is equal to 0
		if(count % 2 === 0){
			// tells the code to continue to the next iteration of the loop
			// this also ignores all statements located after a continue statement
			continue;
		}
		console.log('Continue and Break:'+count);

		if(count>10){
			// tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of cunt is less than or equal to 20
			// number values greater than 10 will no longer be printed.
			break;
		}
	}
/*
	- Creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
    - How this For Loop works:
        1. The loop will start at 0 for the the value of "count".
        2. It will check if "count" is less than the or equal to 20.
        3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2).
        4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
        5. If the value of count is not equal to 0, the console will print the value of "count".
        6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
        7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
        8. The value of "count" will be incremented by 1 (e.g. count = 1)
        9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] > 10) is true, the loop will stop due to the "break" statemen
*/